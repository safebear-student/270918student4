/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7307692307692307, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [0.75, 500, 1500, "Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "66 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "72 /index.php"], "isController": false}, {"data": [0.75, 500, 1500, "35 /"], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [0.0, 500, 1500, "49 /"], "isController": false}, {"data": [1.0, 500, 1500, "Go to Risk management"], "isController": true}, {"data": [1.0, 500, 1500, "Submit new risk with file attachment"], "isController": true}, {"data": [1.0, 500, 1500, "50 /reports"], "isController": false}, {"data": [0.0, 500, 1500, "Submit risk"], "isController": true}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "56 /management/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "71 /logout.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 28, 0, 0.0, 2109.6785714285716, 14, 13781, 13716.0, 13767.95, 13781.0, 0.5401755570560431, 2.877350456978875, 0.8116198032217613], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["Open Login Page", 4, 0, 0.0, 920.4999999999999, 20, 3352, 3352.0, 3352.0, 3352.0, 0.13760363273590423, 0.35254211961195775, 0.04864503422890364], "isController": true}, {"data": ["66 /management/index.php", 4, 0, 0.0, 99.25, 74, 122, 122.0, 122.0, 122.0, 0.14066676044450696, 1.3454693214587143, 0.8885770897805598], "isController": false}, {"data": ["72 /index.php", 4, 0, 0.0, 20.25, 14, 29, 29.0, 29.0, 29.0, 0.14120803473717655, 0.37501489303491364, 0.05708996716913192], "isController": false}, {"data": ["35 /", 4, 0, 0.0, 920.4999999999999, 20, 3352, 3352.0, 3352.0, 3352.0, 0.13876361617983765, 0.3555140107888711, 0.04905510650107542], "isController": false}, {"data": ["Login", 4, 0, 0.0, 13649.5, 13241, 13819, 13819.0, 13819.0, 13819.0, 0.09494422027059102, 1.069629161227154, 0.20820043615001185], "isController": true}, {"data": ["49 /", 4, 0, 0.0, 13608.75, 13190, 13781, 13781.0, 13781.0, 13781.0, 0.09503219215509254, 0.6273145535862773, 0.13201493490294838], "isController": false}, {"data": ["Go to Risk management", 4, 0, 0.0, 38.0, 36, 42, 42.0, 42.0, 42.0, 0.14081036364276409, 1.1227013808216284, 0.059404372161791105], "isController": true}, {"data": ["Submit new risk with file attachment", 4, 0, 0.0, 99.25, 74, 122, 122.0, 122.0, 122.0, 0.14066181383408938, 1.3454220074199106, 0.8885458425642648], "isController": true}, {"data": ["50 /reports", 4, 0, 0.0, 40.75, 35, 51, 51.0, 51.0, 51.0, 0.14080045056144178, 0.6568052267767257, 0.1131628621211588], "isController": false}, {"data": ["Submit risk", 4, 0, 0.0, 14767.75, 14038, 16753, 16753.0, 16753.0, 16753.0, 0.09290010915762825, 3.4639538504192116, 0.9770841558863831], "isController": true}, {"data": ["Logout", 4, 0, 0.0, 60.5, 50, 77, 77.0, 77.0, 77.0, 0.14100394811054712, 0.8348700560490694, 0.17377635011280318], "isController": true}, {"data": ["56 /management/index.php", 4, 0, 0.0, 38.0, 36, 42, 42.0, 42.0, 42.0, 0.1408153207068929, 1.1227409042103782, 0.05940646342322045], "isController": false}, {"data": ["71 /logout.php", 4, 0, 0.0, 40.25, 34, 48, 48.0, 48.0, 48.0, 0.14106859460412627, 0.46060824810439077, 0.11682242990654206], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 28, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
